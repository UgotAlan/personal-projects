﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AppLayer
{
    public class LockedBox :Strategy
    {
        private int? _box = null;
        private List<int> _row = new List<int>();
        private List<int> _col = new List<int>();
        private List<char> temp = new List<char>();
        private int boxSize;
        private int StartRow;
        private int EndRow;
        private int StartColumn;
        private int EndColumn;

        public override void Reset()
        {
            _madeProgress = false;
            _box = null;
            _row.Clear();
            _col.Clear();
            temp.Clear();
            boxSize = (int)Math.Sqrt(_puzzle.Size);
        }

        public override bool FindElement()
        {
            int start = 0;
            if (_box != null)
                start = (int)_box + 1;

            for (int box = start; box < _puzzle.Size; box++)
            {
                // for this we only want to look at a box that has open elements
                if (_puzzle.Boxes.ElementAt(box).Count < _puzzle.Symbols.Count )
                {
                    _box = box;
                    StartRow = (box / boxSize) * boxSize;
                    EndRow = StartRow + boxSize;
                    StartColumn = (box % boxSize) * boxSize;
                    EndColumn = StartColumn + boxSize;
                    return true;
                }
            }
            return false;
        }

        public override bool FindCondition()
        {
            temp.Clear();
            _row.Clear();
            _col.Clear();

            if (CheckLockedRow())
                return true;

            if (CheckLockedColumn())
                return true;

            return false;
        }

        private bool CheckLockedColumn()
        {
            for (int col = StartColumn; col < EndColumn; col++)
            {
                Dictionary<char, int> count = new Dictionary<char, int>();
                for (int row = StartRow; row < EndRow; row++)
                {
                    if (_puzzle.AvailableOptions[row, col] != null)
                    {
                        foreach (char symbol in _puzzle.AvailableOptions[row, col])
                        {
                            if (count.ContainsKey(symbol))
                                count[symbol]++;
                            else
                                count.Add(symbol, 1);
                        }
                    }
                }
                foreach (KeyValuePair<char, int> symbol in count)
                {
                    if (symbol.Value > 1)
                    {
                        bool flag = true;
                        for (int r = StartRow; r < EndRow; r++)
                        {
                            for (int c = StartColumn; c < EndColumn; c++)
                            {
                                if (c != col && _puzzle.AvailableOptions[r, c] != null)
                                {
                                    foreach (char symb in _puzzle.AvailableOptions[r, c])
                                    {
                                        if (symb == symbol.Key)
                                            flag = false;
                                    }
                                }
                            }
                        }
                        if (flag)
                        {
                            temp.Add(symbol.Key);
                            _col.Add(col);
                        }
                    }
                    
                }
            }
            if (temp.Count == 0 || temp.Count != _col.Count)
                return false;
            else
                return true;
        }

        private bool CheckLockedRow()
        {
            for (int row = StartRow; row < EndRow; row++)
            {
                Dictionary<char, int> count = new Dictionary<char, int>();
                for (int col = StartColumn; col < EndColumn; col++)
                {
                    if (_puzzle.AvailableOptions[row, col] != null)
                    {
                        foreach (char symbol in _puzzle.AvailableOptions[row, col])
                        {
                            if (count.ContainsKey(symbol))
                                count[symbol]++;
                            else
                                count.Add(symbol, 1);
                        }
                    }
                }
                foreach (KeyValuePair<char, int> symbol in count)
                {
                    if (symbol.Value > 1)
                    {
                        bool flag = true;
                        for (int r = StartRow; r < EndRow; r++)
                        {
                            if (r != row)
                            {
                                for (int c = StartColumn; c < EndColumn; c++)
                                {
                                    if (_puzzle.AvailableOptions[r,c] != null)
                                    {
                                        foreach (char symb in _puzzle.AvailableOptions[r, c])
                                        {
                                            if (symb == symbol.Key)
                                                flag = false;
                                        }
                                    }
                                }
                            }
                        }
                        if (flag)
                        {
                            temp.Add(symbol.Key);
                            _row.Add(row);
                        }
                    }

                }
            }
            if (temp.Count == 0 || temp.Count != _row.Count)
                return false;
            else
                return true;
        }

        public override void Update()
        {
            if (temp == null) return;

            if (_row.Count > 0)
                UpdateRow();

            if (_col.Count > 0)
                UpdateCol();

        }

        private void UpdateCol()
        {
            for (int i = 0; i < temp.Count; i++)
            {
                for (int r = 0; r < _puzzle.Size; r++)
                {
                    if (r < StartRow || r >= EndRow)
                    {
                        if (_puzzle.AvailableOptions[r, _col[i]] != null)
                        {
                            if (_puzzle.AvailableOptions[r, _col[i]].Remove(temp[i]))
                                _madeProgress = true;
                        }   
                    }
                }
            }
        }

        private void UpdateRow()
        {
            for (int i = 0; i < temp.Count; i++)
            {
                for (int c = 0; c < _puzzle.Size; c++)
                {
                    if (c < StartColumn || c >= EndColumn)
                    {
                        if (_puzzle.AvailableOptions[_row[i], c] != null)
                        {
                            if(_puzzle.AvailableOptions[_row[i], c].Remove(temp[i]))
                                _madeProgress = true;
                        }
                    }
                }
            }
        }
    }
}
