﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AppLayer
{
    public class LockedRowColumn : Strategy
    {
        private int? _row = null;
        private int? _col = null;
        private List<char> temp = new List<char>();
        private int boxSize;
        private List<int> StartRow = new List<int>();
        private List<int> EndRow = new List<int>();
        private List<int> StartColumn = new List<int>();
        private List<int> EndColumn = new List<int>();

        public override void Reset()
        {
            _madeProgress = false;
            _row = null;
            _col = null;
            temp.Clear();
            StartRow.Clear();
            EndRow.Clear();
            StartColumn.Clear();
            EndColumn.Clear();
            boxSize = (int)Math.Sqrt(_puzzle.Size);
        }

        public override bool FindElement()
        {
            if (FindRow())
                return true;

            if (FindColumn())
                return true;

            return false;
        }

        private bool FindColumn()
        {
            int start;
            if (_col == null)
                start = 0;
            else if (_col < _puzzle.Size)
                start = (int)_col + 1;
            else
                start = (int) _col;

            for (int col = start; col < _puzzle.Size; col++)
            {
                if (_puzzle.Columns[col].Count < _puzzle.Symbols.Count)
                {
                    _col = col;
                    return true;
                }
            }
            _col = start;
            return false;
        }

        private bool FindRow()
        {
            int start;
            if (_row == null)
                start = 0;
            else if (_row < _puzzle.Size)
                start = (int)_row + 1;
            else
                start = (int) _row;

            for (int row = start; row < _puzzle.Size; row++)
            {
                if (_puzzle.Rows[row].Count < _puzzle.Symbols.Count)
                {
                    _row = row;
                    return true;
                }
            }
            _row = start;
            return false;
        }

        public override bool FindCondition()
        {
            temp.Clear();
            StartRow.Clear();
            EndRow.Clear();
            StartColumn.Clear();
            EndColumn.Clear();

            if (FindLockedRow())
                return true;

            if (FindLockedColumn())
                return true;

            return false;
        }

        private bool FindLockedColumn()
        {
            if (_col == null || _col >= _puzzle.Size) return false;

            Dictionary<char, int> count = new Dictionary<char, int>();

            for (int row = 0; row < _puzzle.Size; row++)
            {
                if (_puzzle.AvailableOptions[row, (int) _col] != null)
                {
                    foreach (char symbol in _puzzle.AvailableOptions[row, (int) _col])
                    {
                        if (count.ContainsKey(symbol))
                            count[symbol]++;
                        else
                            count.Add(symbol, 1);
                    }
                }
            }
            foreach (KeyValuePair<char,int> symbol in count)
            {
                if (symbol.Value > 1 && symbol.Value <= boxSize)
                {
                    bool flag = true;
                    int? rowBox = null;
                    for (int row = 0; row < _puzzle.Size; row++)
                    {
                        if (_puzzle.AvailableOptions[row, (int)_col] != null)
                        {
                            if (_puzzle.AvailableOptions[row, (int)_col].Contains(symbol.Key))
                            {
                                if (rowBox == null)
                                    rowBox = row / boxSize;
                                else if (rowBox != row / boxSize)
                                    flag = false;
                            }
                        }
                    }
                    if (flag)
                    {
                        temp.Add(symbol.Key);
                        StartRow.Add(((int)rowBox) * boxSize);
                        EndRow.Add(StartRow.Last() + boxSize);
                        StartColumn.Add(((int) _col / boxSize) * boxSize);
                        EndColumn.Add(StartColumn.Last() + boxSize);
                    }
                }
            }
            if (temp.Count == 0)
                return false;
            else
                return true;
        }

        private bool FindLockedRow()
        {
            if (_row == null || _row >= _puzzle.Size) return false;

            Dictionary<char, int> count = new Dictionary<char, int>();

            for (int col = 0; col < _puzzle.Size; col++)
            {
                if (_puzzle.AvailableOptions[(int) _row, col] != null)
                {
                    foreach (char symbol in _puzzle.AvailableOptions[(int) _row, col])
                    {
                        if (count.ContainsKey(symbol))
                            count[symbol]++;
                        else
                            count.Add(symbol, 1);
                    }
                }
            }
            foreach (KeyValuePair<char, int> symbol in count)
            {
                if (symbol.Value > 1 && symbol.Value <= boxSize)
                {
                    bool flag = true;
                    int? columnBox = null;
                    for (int col = 0; col < _puzzle.Size; col++)
                    {
                        if (_puzzle.AvailableOptions[(int) _row, col] != null)
                        {
                            if (_puzzle.AvailableOptions[(int) _row, col].Contains(symbol.Key))
                            {
                                if (columnBox == null)
                                    columnBox = col / boxSize;
                                else if (columnBox != col / boxSize)
                                    flag = false;
                            }
                        }
                    }
                    if (flag)
                    {
                        temp.Add(symbol.Key);
                        StartRow.Add(((int)_row / boxSize) * boxSize);
                        EndRow.Add(StartRow.Last() + boxSize);
                        StartColumn.Add(((int)columnBox) * boxSize);
                        EndColumn.Add(StartColumn.Last() + boxSize);
                    }
                }
            }
            if (temp.Count == 0)
                return false;
            else
                return true;
        }

        public override void Update()
        {
            if (temp.Count == 0) return;

            if (_row != null && _row < _puzzle.Size)
                UpdateRow();
            if (_col != null && _col < _puzzle.Size)
                UpdateColumn();
        }

        private void UpdateColumn()
        {
            if (_col == null || _col >= _puzzle.Size) return;

            for (int i = 0; i < temp.Count; i++)
            {
                for (int row = StartRow[i]; row < EndRow[i]; row++)
                {
                    for (int col = StartColumn[i]; col < EndColumn[i]; col++)
                    {
                        if (col != _col && _puzzle.AvailableOptions[row, col] != null)
                        {
                            if (_puzzle.AvailableOptions[row, col].Remove(temp[i]))
                                _madeProgress = true;
                        }
                    }
                }
            }
        }

        private void UpdateRow()
        {
            if (_row == null || _row >= _puzzle.Size) return;

            for (int i = 0; i < temp.Count; i++)
            {
                for (int row = StartRow[i]; row < EndRow[i]; row++)
                {
                    if (row != _row)
                    {
                        for (int col = StartColumn[i]; col < EndColumn[i]; col++)
                        {
                            if (_puzzle.AvailableOptions[row, col] != null)
                            {
                                if (_puzzle.AvailableOptions[row, col].Remove(temp[i]))
                                    _madeProgress = true;
                            }
                        }
                    }
                }
            }
        }
    }
}
