﻿using System;
using System.Collections.Generic;

namespace AppLayer
{
    public class Puzzle
    {
        public char[,] MyPuzzle { get; set; }
        public char[,] OriginalPuzzle { get; set; }
        public List<char>[,] AvailableOptions { get; set; }
        public int Size { get; set; }
        public List<char> Symbols { get; set; }
        public List<char>[] Rows { get; set; }
        public List<char>[] Columns { get; set; }
        public List<char>[] Boxes { get; set; }
        public Stack<char[,]> SavedPuzzleState { get; set; } = new Stack<char[,]>();
        public Stack<List<char>[,]> SavedAvailableOptions { get; set; } = new Stack<List<char>[,]>();
        public Stack<char> SavedSymbol { get; set; } = new Stack<char>();
        public List<char[,]> SavedSolvedPuzzle { get; set; } = new List<char[,]>();
        public bool IsSolved { get; set; }
        public bool IsValid { get; set; }
        public int TimesSolved { get; set; }

        public Puzzle()
        {
            IsSolved = false;
            TimesSolved = 0;
            Symbols = new List<char>();
            IsValid = false;
        }

        public void CheckValid()
        {
            if (MyPuzzle == null) return;

            IsValid = true;
            if (!CheckDimentions())
                return;
            if (!CheckSymbols())
                return;
            CheckDuplicates();
        }

        private bool CheckDimentions()
        {
            if (MyPuzzle.GetLength(0) != Size || MyPuzzle.Length != Size * Size)
            {
                IsValid = false;
                return false;
            }

            return true;
        }

        private bool CheckSymbols()
        {
            foreach (char c in MyPuzzle)
            {
                if (!Symbols.Contains(c) && c != '-')
                {
                    IsValid = false;
                    return false;
                }       
            }

            return true;
        }

        private void CheckDuplicates()
        {
            CheckDuplicateRow();
            CheckDuplicateColumn();
            CheckDuplicateBox();            
        }

        private void CheckDuplicateBox()
        {
            int boxSize = (int)Math.Sqrt(Size);

            for ( int i = 0; i < Size; i++)
            {
                List<char> box = new List<char>();
                for (int r = (i / boxSize) * boxSize; r < ((i + boxSize) / boxSize) * boxSize; r++)
                {
                    for(int c = ((i % boxSize) * boxSize); c < ((i % boxSize) * boxSize) + boxSize; c++)
                    {
                        if (box.Contains(MyPuzzle[r, c]))
                        {
                            IsValid = false;
                            return;
                        }
                        else if (MyPuzzle[r, c] != '-')
                            box.Add(MyPuzzle[r, c]);
                    }
                }
            }
        }

        private void CheckDuplicateColumn()
        {
            for (int c = 0; c < Size; c++)
            {
                List<char> column = new List<char>();
                for (int r = 0; r < Size; r++)
                {
                    if (column.Contains(MyPuzzle[r, c]))
                    {
                        IsValid = false;
                        return;
                    }
                    else if (MyPuzzle[r, c] != '-')
                        column.Add(MyPuzzle[r, c]);
                }
            }
        }

        private void CheckDuplicateRow()
        {
            for (int r = 0; r < Size; r++)
            {
                List<char> row = new List<char>();
                for (int c = 0; c < Size; c++)
                {
                    if (row.Contains(MyPuzzle[r, c]))
                    {
                        IsValid = false;
                        return;
                    }
                    else if (MyPuzzle[r, c] != '-')
                        row.Add(MyPuzzle[r, c]);
                }   
            }
        }

        public void SetupAvailable()
        {
            if (MyPuzzle == null) return;

            int boxSize = (int)Math.Sqrt(Size);
            AvailableOptions = new List<char>[Size, Size];
            Rows = new List<char>[Size];
            Columns = new List<char>[Size];
            Boxes = new List<char>[Size];
            // initialize
            for (int i = 0; i < Size; i++)
            {
                Rows[i] = new List<char>();
                Columns[i] = new List<char>();
                Boxes[i] = new List<char>();
            }

            for (int r = 0; r < Size; r++)
            {
                for (int c = 0; c < Size; c++)
                {
                    if (MyPuzzle[r, c] == '-')
                    {
                        // put all available options in this cell
                        AvailableOptions[r, c] = new List<char>();
                        foreach (char symbol in Symbols)
                            AvailableOptions[r, c].Add(symbol);
                    }
                    else
                    {
                        Rows[r].Add(MyPuzzle[r, c]);
                        Columns[c].Add(MyPuzzle[r, c]);
                        Boxes[(r / boxSize) * boxSize + (c / boxSize)].Add(MyPuzzle[r, c]);
                    }
                }
            }
        }
    }
}
