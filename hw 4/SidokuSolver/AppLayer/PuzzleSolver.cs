﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace AppLayer
{
    public class PuzzleSolver
    {
        public Puzzle Puzzle { get; set; } = new Puzzle();
        private List<Strategy> _strategies = new List<Strategy>();
        private int _numGuesses = 0;
        private int _currentStrategy = 0;
        private Stopwatch timer = new Stopwatch();
        private string errorMessage = null;

        public void Solve()
        {
            Puzzle.CheckValid();
            if (!Puzzle.IsValid)
            {
                errorMessage = "Bad Puzzle";
                return;
            }
            // now lets solve this thing!
            Puzzle.SetupAvailable();
            AddStrategies();
            timer.Start();
            do
            {
                if (_strategies[_currentStrategy].Run() && _currentStrategy != 0)
                    _currentStrategy = 0;
                else
                    _currentStrategy++;

                CheckPuzzleState();
                if (_currentStrategy == 5)
                    _numGuesses++;

            } while ((!Puzzle.IsSolved) && Puzzle.IsValid);
            timer.Stop();
        }

        public void Save(string outFilename)
        {
            StreamWriter outputFile = new StreamWriter(outFilename, false);
            if (errorMessage != null)
            {
                PrintPuzzle(Puzzle.MyPuzzle, outputFile);
                outputFile.WriteLine(errorMessage);
            }
            else
            {
                if (!Puzzle.IsValid)
                {
                    PrintPuzzle(Puzzle.OriginalPuzzle, outputFile);
                    if (Puzzle.IsSolved)
                        outputFile.WriteLine("Multiple Solutions");
                    else
                        outputFile.WriteLine("Unsolvable");
                }

                if (Puzzle.SavedSolvedPuzzle.Count > 0)
                {
                    foreach (char[,] puzzle in Puzzle.SavedSolvedPuzzle)
                    {
                        PrintPuzzle(puzzle, outputFile);
                        outputFile.WriteLine();
                    }
                }

                outputFile.WriteLine($"The Puzzle took {timer.ElapsedMilliseconds} ms to run.");
                if (_numGuesses < 5000)
                {
                    if (_numGuesses == 1)
                        outputFile.WriteLine($"There was {_numGuesses} guess.");
                    else
                        outputFile.WriteLine($"There were {_numGuesses} guesses.");
                }
                else
                    outputFile.WriteLine($"Stopped guessing after {_numGuesses} guesses.");
                
            }
            outputFile.Close();
        }

        private void CheckPuzzleState()
        {
            // problem if we get to undo guess on our own...
            if (_currentStrategy == _strategies.Count - 1)
            {
                errorMessage = "INVALID should never get here on it's own...";
                Puzzle.IsValid = false;
            }
            // if have invalid puzzle from guess,  undo and try again
            else if ((!Puzzle.IsValid) && Puzzle.SavedSymbol.Count > 0 && Puzzle.TimesSolved < 2)
            {
                if (_numGuesses < 5000)
                {
                    Puzzle.IsValid = true;
                    _currentStrategy = _strategies.Count - 1;
                }
            }
            // if we have invalid and not a guess check to see if a previous try worked
            else if ((!Puzzle.IsValid) && Puzzle.SavedSolvedPuzzle.Count == 1)
                Puzzle.IsSolved = Puzzle.IsValid = true;
            // if the puzzle was solved we need to check for multiple ways to solve.
            else if (Puzzle.IsSolved)
            {
                // first save the solved puzzle!
                SaveSolvedPuzzle();
                // check if puzzle had to guess.
                if (Puzzle.SavedSymbol.Count > 0)
                {
                    // if puzzle has been solved more than 1 way set is invalid
                    if (Puzzle.SavedSolvedPuzzle.Count > 1)
                        Puzzle.IsValid = false;
                    // this assumes that this is the first time the puzzle was solved. undo guess and try again.
                    else
                    {
                        Puzzle.IsSolved = false;
                        _currentStrategy = _strategies.Count - 1;
                    }
                }
            }            
        }

        private void SaveSolvedPuzzle()
        {
            if (!Puzzle.IsSolved && Puzzle.TimesSolved == 0) return;

            char[,] tempPuzzle = new char[Puzzle.Size, Puzzle.Size];

            for (int r = 0; r < Puzzle.Size; r++)
            {
                for (int c = 0; c < Puzzle.Size; c++)
                {
                    tempPuzzle[r, c] = Puzzle.MyPuzzle[r, c];
                }
            }
            Puzzle.SavedSolvedPuzzle.Add(tempPuzzle);
        }

        private void AddStrategies()
        {
            _strategies.Add(new RemoveAvailable() { _puzzle = Puzzle });
            _strategies.Add(new SetSingle() { _puzzle = Puzzle });
            _strategies.Add(new SetSingleHidden() { _puzzle = Puzzle });
            _strategies.Add(new LockedBox() { _puzzle = Puzzle });
            _strategies.Add(new LockedRowColumn() { _puzzle = Puzzle });
            _strategies.Add(new SetRandom() { _puzzle = Puzzle });
            _strategies.Add(new UndoGuess() { _puzzle = Puzzle });
        }

        public void LoadPuzzle(string filename)
        {
            try
            {
                string[] lines = File.ReadAllLines(filename);
                Puzzle.Size = Int16.Parse(lines[0]);
                Puzzle.MyPuzzle = new char[Puzzle.Size, Puzzle.Size];
                Puzzle.OriginalPuzzle = new char[Puzzle.Size, Puzzle.Size];
                char[] characters = lines[1].ToCharArray();
                foreach (char c in characters)
                {
                    if (c != ' ')
                        Puzzle.Symbols.Add(c);
                }
                if (Puzzle.Symbols.Count != Puzzle.Size)
                    errorMessage = $"Something is not right! {Puzzle.Size} != {Puzzle.Symbols.Count()}";

                for (int row = 0; row < Puzzle.Size; row++)
                {
                    characters = lines[row + 2].ToCharArray();
                    for (int col = 0; col < Puzzle.Size; col++)
                    {
                        Puzzle.MyPuzzle[row, col] = characters[col * 2];
                        Puzzle.OriginalPuzzle[row, col] = characters[col * 2];
                    }
                }
            }
            catch
            {
                errorMessage = "Error reading puzzle. Incorrect format.";
            }
        }

        private void PrintPuzzle(char[,] puzzle, StreamWriter outputFile)
        {
            outputFile.WriteLine(Puzzle.Size);
            foreach (char symbol in Puzzle.Symbols)
                outputFile.Write($"{symbol} ");
            outputFile.WriteLine();

            for (int row = 0; row < Puzzle.Size; row++)
            {
                for (int col = 0; col < Puzzle.Size; col++)
                    outputFile.Write($"{puzzle[row, col]} ");
                outputFile.WriteLine();
            }
        }
    }
}
