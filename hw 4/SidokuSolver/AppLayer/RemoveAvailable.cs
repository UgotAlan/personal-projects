﻿using System;
using System.Collections.Generic;

namespace AppLayer
{
    public class RemoveAvailable : Strategy
    {
        private int _row = 0;
        private int _col = 0;
        List<char> temp = new List<char>();

        public override void Reset()
        {
            _madeProgress = false;
            _row = 0;
            _col = -1;
            temp.Clear();
        }

        public override bool FindElement()
        {
            for ( int r = 0; r < _puzzle.Size; r++)
            {
                for (int c = 0; c < _puzzle.Size; c++)
                {
                    if (_puzzle.MyPuzzle[r, c] == '-')
                    {
                        if ((_row == r && _col < c) || _row < r)
                        {
                            _row = r;
                            _col = c;
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        public override bool FindCondition()
        {
            if (_puzzle.AvailableOptions[_row, _col] == null) return false;

            int boxSize = (int) Math.Sqrt(_puzzle.Size);
            temp.Clear();
            // check if each symbol is contained in row, col or box if yes to any then remove.
            foreach (char symbol in _puzzle.AvailableOptions[_row, _col])
            {
                if (_puzzle.Rows[_row].Contains(symbol) ||
                    _puzzle.Columns[_col].Contains(symbol) ||
                    _puzzle.Boxes[(_row / boxSize) * boxSize + (_col / boxSize)].Contains(symbol))
                {
                    temp.Add(symbol);
                } 
            }

            if (temp.Count > 0)
                return true;
            else
                return false;
        }

        public override void Update()
        {
            if (temp == null) return;
            foreach (char symbol in temp)
            {
                if(_puzzle.AvailableOptions[_row, _col].Remove(symbol))
                    _madeProgress = true;
            } 
        }
    }
}
