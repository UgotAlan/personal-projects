﻿using System;
using System.Collections.Generic;

namespace AppLayer
{
    public class SetRandom : Strategy
    {
        private int _row = 0;
        private int _col = -1;
        char? temp;
        Random random = new Random();

        public override void Reset()
        {
            _madeProgress = false;
            _row = 0;
            _col = -1;
            temp = null;
        }

        public override void SaveState()
        {
            char[,] tempPuzzle = new char[_puzzle.Size, _puzzle.Size];
            List<char>[,] tempAvailable = new List<char>[_puzzle.Size, _puzzle.Size];
            for (int r = 0; r < _puzzle.Size; r++)
            {
                for (int c = 0; c < _puzzle.Size; c++)
                {
                    tempPuzzle[r, c] = _puzzle.MyPuzzle[r, c];
                    if (_puzzle.AvailableOptions[r, c] != null)
                    {
                        tempAvailable[r, c] = new List<char>();
                        foreach (char symbol in _puzzle.AvailableOptions[r, c])
                        {
                            tempAvailable[r, c].Add(symbol);
                        }
                    }
                }
            }
            _puzzle.SavedPuzzleState.Push(tempPuzzle);
            _puzzle.SavedAvailableOptions.Push(tempAvailable);
        }

        public override bool FindElement()
        {
            for (int r = 0; r < _puzzle.Size; r++)
            {
                for (int c = 0; c < _puzzle.Size; c++)
                {
                    if (_puzzle.MyPuzzle[r, c] == '-')
                    {
                        if ((_row == r && _col < c) || _row < r)
                        {
                            _row = r;
                            _col = c;
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        public override bool FindCondition()
        {
            if (_puzzle.AvailableOptions[_row, _col] == null) return false;

            temp = null;

            if (_puzzle.AvailableOptions[_row, _col].Count > 0)
            {
                temp = _puzzle.AvailableOptions[_row, _col][random.Next(_puzzle.AvailableOptions[_row, _col].Count)];
            }
            if (temp != null)
                return true;
            else
                return false;
        }

        public override void Update()
        {
            if (temp == null) return;

            _puzzle.SavedSymbol.Push((char) temp);

            int boxSize = (int)Math.Sqrt(_puzzle.Size);
            _puzzle.MyPuzzle[_row, _col] = (char)temp;
            // check to make sure this doesn't make the puzzle invalid
            if (_puzzle.Rows[_row].Contains((char)temp))
                _puzzle.IsValid = false;
            else
                _puzzle.Rows[_row].Add((char)temp);
            if (_puzzle.Columns[_col].Contains((char)temp))
                _puzzle.IsValid = false;
            else
                _puzzle.Columns[_col].Add((char)temp);
            if (_puzzle.Boxes[(_row / boxSize) * boxSize + (_col / boxSize)].Contains((char)temp))
                _puzzle.IsValid = false;
            else
                _puzzle.Boxes[(_row / boxSize) * boxSize + (_col / boxSize)].Add((char)temp);
            _puzzle.AvailableOptions[_row, _col] = null;
            // need to set _row and _col to max size to break the loop
            _row = _col = _puzzle.Size;
            _madeProgress = true;
        }
    }
}
