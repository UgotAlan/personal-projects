﻿using System;

namespace AppLayer
{
    public class SetSingle : Strategy
    {
        private int _row = 0;
        private int _col = -1;
        char? temp;

        public override void Reset()
        {
            _madeProgress = false;
            _row = 0;
            _col = 0;
            temp = null;
        }

        public override bool FindElement()
        {
            for (int r = 0; r < _puzzle.Size; r++)
            {
                for (int c = 0; c < _puzzle.Size; c++)
                {
                    if (_puzzle.MyPuzzle[r, c] == '-')
                    {
                        if ((_row == r && _col < c) || _row < r)
                        {
                            _row = r;
                            _col = c;
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        public override bool FindCondition()
        {
            if (_puzzle.AvailableOptions[_row, _col] == null) return false;

            temp = null;

            if (_puzzle.AvailableOptions[_row, _col].Count == 1)
            {
                temp = _puzzle.AvailableOptions[_row, _col][0];
            }
            else if (_puzzle.AvailableOptions[_row, _col].Count == 0)
                _puzzle.IsValid = false;
            if (temp != null)
                return true;
            else
                return false;
        }

        public override void Update()
        {
            if (temp == null) return;

            int boxSize = (int)Math.Sqrt(_puzzle.Size);
            _puzzle.MyPuzzle[_row, _col] = (char) temp;
            // check to make sure this doesn't make the puzzle invalid
            if (_puzzle.Rows[_row].Contains((char) temp))
                _puzzle.IsValid = false;
            else
                _puzzle.Rows[_row].Add((char) temp);
            if (_puzzle.Columns[_col].Contains((char) temp))
                _puzzle.IsValid = false;
            else
                _puzzle.Columns[_col].Add((char) temp);
            if (_puzzle.Boxes[(_row / boxSize) * boxSize + (_col / boxSize)].Contains((char)temp))
                _puzzle.IsValid = false;
            else
                _puzzle.Boxes[(_row / boxSize) * boxSize + (_col / boxSize)].Add((char)temp);
            _puzzle.AvailableOptions[_row, _col] = null;
            _madeProgress = true;
            _row = _col = _puzzle.Size;

            if (checkIfSolved())
            {
                _puzzle.TimesSolved++;
                _puzzle.IsSolved = true;
            }
        }

        private bool checkIfSolved()
        {
            bool solved = true;

            foreach (char symbol in _puzzle.MyPuzzle)
            {
                if (symbol == '-')
                    solved = false;
            }
            return solved;
        }
    }
}
