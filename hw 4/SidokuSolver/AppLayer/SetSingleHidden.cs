﻿using System;

namespace AppLayer
{
    public class SetSingleHidden : Strategy
    {
        private int _row = 0;
        private int _col = 0;
        char? temp;

        public override void Reset()
        {
            _madeProgress = false;
            _row = 0;
            _col = -1;
            temp = null;
        }

        public override bool FindElement()
        {
            for (int r = 0; r < _puzzle.Size; r++)
            {
                for (int c = 0; c < _puzzle.Size; c++)
                {
                    if (_puzzle.MyPuzzle[r, c] == '-')
                    {
                        if ((_row == r && _col < c) || _row < r)
                        {
                            _row = r;
                            _col = c;
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        public override bool FindCondition()
        {
            if (_puzzle.AvailableOptions[_row, _col] == null) return false;

            temp = null;

            foreach (char symbol in _puzzle.AvailableOptions[_row, _col])
            {
                if (findRowCount(symbol) == 1)
                {
                    temp = symbol;
                    return true;
                } 
                if (findColumnCount(symbol) == 1)
                {
                    temp = symbol;
                    return true;
                }
                if (findBoxCount(symbol) == 1)
                {
                    temp = symbol;
                    return true;
                }
            }
            return false;
        }

        private int findBoxCount(char symbol)
        {
            // figure out the size and starting spot of the box
            int boxSize = (int)Math.Sqrt(_puzzle.Size);
            int startRow = (_row / boxSize) * boxSize;
            int endRow = startRow + boxSize;
            int startCol = (_col / boxSize) * boxSize;
            int endCol = startCol + boxSize;

            int count = 0;
            for (int r = startRow; r < endRow; r++)
            {
                for (int c = startCol; c < endCol; c++)
                {
                    if (_puzzle.MyPuzzle[r, c] == '-')
                    {
                        foreach (char symb in _puzzle.AvailableOptions[r, c])
                        {
                            if (symb == symbol)
                                count++;
                        }
                    }
                }
            }
            return count;
        }

        private int findColumnCount(char symbol)
        {
            int count = 0;
            for (int r = 0; r < _puzzle.Size; r++)
            {
                if (_puzzle.MyPuzzle[r, _col] == '-')
                {
                    foreach (char symb in _puzzle.AvailableOptions[r, _col])
                    {
                        if (symb == symbol)
                            count++;
                    }
                }
            }
            return count;
        }

        private int findRowCount(char symbol)
        {
            int count = 0;
            for (int c = 0; c < _puzzle.Size; c ++)
            {
                if (_puzzle.MyPuzzle[_row, c] == '-')
                {
                    foreach (char symb in _puzzle.AvailableOptions[_row, c])
                    {
                        if (symb == symbol)
                            count++;
                    }
                }
            }
            return count;
        }

        public override void Update()
        {
            if (temp == null) return;

            int boxSize = (int)Math.Sqrt(_puzzle.Size);
            _puzzle.MyPuzzle[_row, _col] = (char)temp;
            // check to make sure this doesn't make the puzzle invalid
            if (_puzzle.Rows[_row].Contains((char)temp))
                _puzzle.IsValid = false;
            else
                _puzzle.Rows[_row].Add((char)temp);

            if (_puzzle.Columns[_col].Contains((char)temp))
                _puzzle.IsValid = false;
            else
                _puzzle.Columns[_col].Add((char)temp);

            if (_puzzle.Boxes[(_row / boxSize) * boxSize + (_col / boxSize)].Contains((char)temp))
                _puzzle.IsValid = false;
            else
                _puzzle.Boxes[(_row / boxSize) * boxSize + (_col / boxSize)].Add((char)temp);
            _puzzle.AvailableOptions[_row, _col] = null;
            _madeProgress = true;
            _row = _col = _puzzle.Size;

            if (checkIfSolved())
            {
                _puzzle.TimesSolved++;
                _puzzle.IsSolved = true;
            }
        }

        private bool checkIfSolved()
        {
            bool solved = true;

            foreach (char symbol in _puzzle.MyPuzzle)
            {
                if (symbol == '-')
                    solved = false;
            }
            return solved;
        }

    }
}
