﻿namespace AppLayer
{
    public abstract class Strategy
    {
        public Puzzle _puzzle = new Puzzle();
        protected bool _madeProgress;

        public bool Run()
        {
            Reset();
            SaveState();
            while(FindElement())
            {
                if(FindCondition())
                {
                    Update();
                }
            }
            if (_madeProgress)
                return true;
            else
                return false;
        }
        public virtual void Reset()
        {
            _madeProgress = false;
        }

        public virtual void SaveState()
        {
            return;
        }

        public virtual bool FindElement()
        {
            return false;
        }

        public virtual bool FindCondition()
        {
            return false;
        }

        public virtual void Update()
        {
            _madeProgress = false;
        }
    }
}
