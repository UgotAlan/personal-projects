﻿namespace AppLayer
{
    public class UndoGuess : Strategy
    {
        private int _row;
        private int _col;
        private char _symbol;

        public override void Reset()
        {
            _puzzle.MyPuzzle = _puzzle.SavedPuzzleState.Pop();
            _puzzle.AvailableOptions = _puzzle.SavedAvailableOptions.Pop();
            _puzzle.SetupAvailable();
            _symbol = _puzzle.SavedSymbol.Pop();
            _madeProgress = false;
            _row = 0;
            _col = -1;
        }

        public override bool FindElement()
        {
            for (int r = 0; r < _puzzle.Size; r++)
            {
                for (int c = 0; c < _puzzle.Size; c++)
                {
                    if (_puzzle.MyPuzzle[r, c] == '-')
                    {
                        if ((_row == r && _col < c) || _row < r)
                        {
                            _row = r;
                            _col = c;
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        public override bool FindCondition()
        {
            if (_puzzle.AvailableOptions[_row, _col] == null) return false;

            if (_puzzle.AvailableOptions[_row, _col].Contains(_symbol))
                return true;

            return false;
        }

        public override void Update()
        {
            if (_puzzle.AvailableOptions[_row, _col].Remove(_symbol))
                _madeProgress = true;
            _row = _col = _puzzle.Size;
        }
    }
}
