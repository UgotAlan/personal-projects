﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using AppLayer;

namespace AppLayerTesting
{
    [TestClass]
    public class LockedBoxTester
    {
        [TestMethod]
        public void LockedBox_Valid()
        {
            Puzzle puzzle = new Puzzle();
            puzzle.Size = 9;
            puzzle.Symbols.Add('1');
            puzzle.Symbols.Add('2');
            puzzle.Symbols.Add('3');
            puzzle.Symbols.Add('4');
            puzzle.Symbols.Add('5');
            puzzle.Symbols.Add('6');
            puzzle.Symbols.Add('7');
            puzzle.Symbols.Add('8');
            puzzle.Symbols.Add('9');
            // just build out the available and then fake the cound of boxes...
            puzzle.AvailableOptions = new List<char>[,]
            {
                { null, null, null,
                    null, null, null,
                    null, null, null },
                { null, null, null,
                    null, null, null,
                    null, null, null },
                { null, null, null,
                    null, null, null,
                    null, null, null },
                { new List<char> {'1', '6' }, new List<char> { '1', '5', '7', '8' }, new List<char> { '1', '5', '7' },
                    null, new List<char> { '1', '2', '5', '7', '8' }, new List<char> { '1', '2', '5', '8' },
                    null, null, new List<char> {'1', '5', '6', '8' } },
                { new List<char> { '1', '3', '4' }, new List<char> { '1', '3', '4', '5', '7', '8' }, null,
                    null, new List<char> { '1', '4', '5', '7', '8' }, new List<char> { '1', '5', '8' },
                    null, new List<char> { '5', '8' }, new List<char> { '1', '5', '8' } },
                { new List<char> { '1', '4', '6' }, null, new List<char> { '1', '4', '5' },
                    new List<char> { '2', '4', '5' }, new List<char> { '1', '2', '4', '5', '8' }, null,
                    new List<char> { '2', '6', '8' }, new List<char> { '2', '5', '6', '8' }, null },
                { null, null, null,
                    null, null, null,
                    null, null, null },
                { null, null, null,
                    null, null, null,
                    null, null, null },
                { null, null, null,
                    null, null, null,
                    null, null, null }
            };
            puzzle.Boxes = new List<char>[9];

            puzzle.Boxes[0] = puzzle.Symbols;
            puzzle.Boxes[1] = puzzle.Symbols;
            puzzle.Boxes[2] = puzzle.Symbols;
            puzzle.Boxes[3] = new List<char> { '2', '9' };
            puzzle.Boxes[4] = new List<char> { '9', '6', '3' };
            puzzle.Boxes[5] = new List<char> { '4', '3', '9', '7' };
            puzzle.Boxes[6] = puzzle.Symbols;
            puzzle.Boxes[6] = puzzle.Symbols;
            puzzle.Boxes[7] = puzzle.Symbols;
            puzzle.Boxes[8] = puzzle.Symbols;

            Strategy strategy = new LockedBox() { _puzzle = puzzle };
            Assert.AreEqual(3, puzzle.AvailableOptions[5, 3].Count);
            Assert.AreEqual(5, puzzle.AvailableOptions[5, 4].Count);
            Assert.AreEqual(3, puzzle.AvailableOptions[5, 6].Count);
            Assert.AreEqual(4, puzzle.AvailableOptions[5, 7].Count);
            strategy.Run();
            // this should remove the 2's on 5th row col 3 and 4
            Assert.AreEqual(2, puzzle.AvailableOptions[5, 3].Count);
            Assert.AreEqual(4, puzzle.AvailableOptions[5, 4].Count);
            Assert.AreEqual(3, puzzle.AvailableOptions[5, 6].Count);
            Assert.AreEqual(4, puzzle.AvailableOptions[5, 7].Count);
        }
    }
}
