﻿using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AppLayer;

namespace AppLayerTesting
{
    [TestClass]
    public class LockedRowColumnTester
    {
        [TestMethod]
        public void LockedRow_Valid()
        {
            Puzzle puzzle = new Puzzle();
            puzzle.Size = 9;
            puzzle.Symbols.Add('1');
            puzzle.Symbols.Add('2');
            puzzle.Symbols.Add('3');
            puzzle.Symbols.Add('4');
            puzzle.Symbols.Add('5');
            puzzle.Symbols.Add('6');
            puzzle.Symbols.Add('7');
            puzzle.Symbols.Add('8');
            puzzle.Symbols.Add('9');
            // just build out the available and then fake the cound of boxes...
            puzzle.AvailableOptions = new List<char>[,]
            {
                { null, null, null,
                    null, new List<char> { '1', '5' }, null,
                    null, null, null },
                { null, null, null,
                    new List<char> {'2', '3', '5' }, null, new List<char> {'1', '3', '5' },
                    null, null, null },
                { null, null, null,
                    null, new List<char> { '1', '2', '5', '9' }, new List<char> {'1', '5', '9' },
                    null, null, null },
                { null, null, null,
                    new List<char> { '5', '7', '9' }, new List<char> { '1', '4', '5', '7', '9' }, new List<char> { '1', '4', '5', '6', '9' },
                    null, null, null },
                { null, null, null,
                    new List<char> { '2', '7', }, null, new List<char> { '1', '8' },
                    null, null, null },
                { null, null, null,
                    new List<char> { '2', '5', '9' }, new List<char> { '2', '5', '9' }, new List<char> { '5', '6', '8', '9' },
                    null, null, null },
                { null, null, null,
                    null, new List<char> { '4', '5', '7', '9' }, new List<char> { '3', '4', '5', '9' },
                    null, null, null },
                { null, null, null,
                    new List<char> { '3', '5', '7' }, null, new List<char> { '3', '4', '5' },
                    null, null, null },
                { null, null, null,
                    null, new List<char> { '4', '5' }, null,
                    null, null, null }
            };
            // act like all rows are full so we don't test them.
            puzzle.Rows = new List<char>[9];
            for (int i = 0; i < puzzle.Size; i++)
                puzzle.Rows[i] = puzzle.Symbols;
            // only test these 3 columns
            puzzle.Columns = new List<char>[9];
            puzzle.Columns[0] = puzzle.Symbols;
            puzzle.Columns[1] = puzzle.Symbols;
            puzzle.Columns[2] = puzzle.Symbols;
            puzzle.Columns[3] = new List<char> { '4', '6', '1', '8' };
            puzzle.Columns[4] = new List<char> { '8', '3', '6' };
            puzzle.Columns[5] = new List<char> { '7', '2' };
            puzzle.Columns[6] = puzzle.Symbols;
            puzzle.Columns[6] = puzzle.Symbols;
            puzzle.Columns[7] = puzzle.Symbols;
            puzzle.Columns[8] = puzzle.Symbols;

            Strategy strategy = new LockedRowColumn() { _puzzle = puzzle };
            Assert.AreEqual(3, puzzle.AvailableOptions[3, 3].Count);
            Assert.AreEqual(5, puzzle.AvailableOptions[3, 4].Count);
            Assert.AreEqual(5, puzzle.AvailableOptions[3, 5].Count);
            Assert.AreEqual(2, puzzle.AvailableOptions[4, 3].Count);
            Assert.AreEqual(2, puzzle.AvailableOptions[4, 5].Count);
            Assert.AreEqual(3, puzzle.AvailableOptions[5, 3].Count);
            Assert.AreEqual(3, puzzle.AvailableOptions[5, 4].Count);
            Assert.AreEqual(4, puzzle.AvailableOptions[5, 5].Count);
            strategy.Run();
            // this should remove the 9's on middle box's right two columns
            Assert.AreEqual(3, puzzle.AvailableOptions[3, 3].Count);
            Assert.AreEqual(4, puzzle.AvailableOptions[3, 4].Count);
            Assert.AreEqual(4, puzzle.AvailableOptions[3, 5].Count);
            Assert.AreEqual(2, puzzle.AvailableOptions[4, 3].Count);
            Assert.AreEqual(2, puzzle.AvailableOptions[4, 5].Count);
            Assert.AreEqual(3, puzzle.AvailableOptions[5, 3].Count);
            Assert.AreEqual(2, puzzle.AvailableOptions[5, 4].Count);
            Assert.AreEqual(3, puzzle.AvailableOptions[5, 5].Count);
        }
    }
}
