﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

using AppLayer;

namespace AppLayerTesting
{
    [TestClass]
    public class PuzzleTester
    {
        [TestMethod]
        public void Puzzle_setup()
        {
            Puzzle puzzle = new Puzzle();
            puzzle.Size = 4;
            puzzle.Symbols.Add('1');
            puzzle.Symbols.Add('2');
            puzzle.Symbols.Add('3');
            puzzle.Symbols.Add('4');
            puzzle.MyPuzzle = new char[,] 
            { 
                { '2', '-', '3', '1' }, 
                { '1', '3', '-', '4' }, 
                { '3', '1', '4', '-' }, 
                { '-', '2', '1', '3' }
            };
            puzzle.SetupAvailable();
            // check that MyPuzzle has the right values... test a few
            Assert.AreEqual('2', puzzle.MyPuzzle[0, 0]);
            Assert.AreEqual('3', puzzle.MyPuzzle[1, 1]);
            Assert.AreEqual('-', puzzle.MyPuzzle[1, 2]);
            // check size
            Assert.AreEqual(4, puzzle.Size);
            // check the 4 open cells that should have count of 4
            Assert.AreEqual(4, puzzle.AvailableOptions[0, 1].Count);
            Assert.AreEqual(4, puzzle.AvailableOptions[1, 2].Count);
            Assert.AreEqual(4, puzzle.AvailableOptions[2, 3].Count);
            Assert.AreEqual(4, puzzle.AvailableOptions[3, 0].Count);
            // check the count for row, column and box, each sould have found of 3
            Assert.AreEqual(3, puzzle.Rows[0].Count);
            Assert.AreEqual(3, puzzle.Rows[1].Count);
            Assert.AreEqual(3, puzzle.Rows[2].Count);
            Assert.AreEqual(3, puzzle.Rows[3].Count);
            Assert.AreEqual(3, puzzle.Columns[0].Count);
            Assert.AreEqual(3, puzzle.Columns[1].Count);
            Assert.AreEqual(3, puzzle.Columns[2].Count);
            Assert.AreEqual(3, puzzle.Columns[3].Count);
            Assert.AreEqual(3, puzzle.Boxes[0].Count);
            Assert.AreEqual(3, puzzle.Boxes[1].Count);
            Assert.AreEqual(3, puzzle.Boxes[2].Count);
            Assert.AreEqual(3, puzzle.Boxes[3].Count);
        }

        [TestMethod]
        public void Puzzle_Valid()
        {
            Puzzle puzzle = new Puzzle();
            puzzle.Size = 4;
            puzzle.Symbols.Add('1');
            puzzle.Symbols.Add('2');
            puzzle.Symbols.Add('3');
            puzzle.Symbols.Add('4');
            puzzle.MyPuzzle = new char[,]
            {
                { '4', '-', '-', '1' },
                { '-', '-', '-', '2' },
                { '3', '-', '-', '-' },
                { '-', '-', '-', '3' }
            };
            puzzle.CheckValid();
            Assert.IsTrue(puzzle.IsValid);
        }

        [TestMethod]
        public void Puzzle_InvalidDuplicate()
        {
            Puzzle puzzle = new Puzzle();
            puzzle.Size = 4;
            puzzle.Symbols.Add('1');
            puzzle.Symbols.Add('2');
            puzzle.Symbols.Add('3');
            puzzle.Symbols.Add('4');
            puzzle.MyPuzzle = new char[,]
            {
                { '4', '4', '-', '1' },
                { '-', '-', '-', '2' },
                { '3', '-', '-', '-' },
                { '-', '-', '4', '3' }
            };
            puzzle.CheckValid();
            Assert.IsFalse(puzzle.IsValid);
        }

        [TestMethod]
        public void Puzzle_InvalidSymbol()
        {
            Puzzle puzzle = new Puzzle();
            puzzle.Size = 4;
            puzzle.Symbols.Add('1');
            puzzle.Symbols.Add('2');
            puzzle.Symbols.Add('3');
            puzzle.Symbols.Add('4');
            puzzle.MyPuzzle = new char[,]
            {
                { '4', '9', '-', '1' },
                { '-', '-', '-', '2' },
                { '3', '-', '2', '-' },
                { '-', '4', '-', '3' }
            };
            puzzle.CheckValid();
            Assert.IsFalse(puzzle.IsValid);
        }

        [TestMethod]
        public void Puzzle_InvalidNull()
        {
            Puzzle puzzle = new Puzzle();
            puzzle.Size = 4;
            puzzle.Symbols.Add('1');
            puzzle.Symbols.Add('2');
            puzzle.Symbols.Add('3');
            puzzle.Symbols.Add('4');
            // never create MyPuzzle
            puzzle.CheckValid();
            // since there is no puzzle then should always return false
            Assert.IsFalse(puzzle.IsValid);
        }

        [TestMethod]
        public void Puzzle_NotEnoughDimentions()
        {
            Puzzle puzzle = new Puzzle();
            puzzle.Size = 4;
            puzzle.Symbols.Add('A');
            puzzle.Symbols.Add('B');
            puzzle.Symbols.Add('C');
            puzzle.Symbols.Add('D');
            puzzle.MyPuzzle = new char[,]
            {
                { 'B', 'D', 'C', 'A' },
                { 'A', 'C', 'B', 'D' }
            };
            puzzle.CheckValid();
            Assert.IsFalse(puzzle.IsValid);
        }

        [TestMethod]
        public void Puzzle_TooManyDimentions()
        {
            Puzzle puzzle = new Puzzle();
            puzzle.Size = 4;
            puzzle.Symbols.Add('A');
            puzzle.Symbols.Add('B');
            puzzle.Symbols.Add('C');
            puzzle.Symbols.Add('D');
            puzzle.MyPuzzle = new char[,]
            {
                { 'B', '-', 'C', 'A' },
                { 'A', 'C', '-', 'D' },
                { 'C', 'A', 'D', '-' },
                { '-', 'D', 'A', 'C' },
                { '-', 'D', 'A', 'C' }
            };
            puzzle.CheckValid();
            Assert.IsFalse(puzzle.IsValid);
        }

        [TestMethod]
        public void Puzzle_BlankSymbols()
        {
            Puzzle puzzle = new Puzzle();
            puzzle.Size = 4;
            puzzle.Symbols.Add('1');
            puzzle.Symbols.Add('2');
            puzzle.Symbols.Add('3');
            puzzle.Symbols.Add('4');
            puzzle.MyPuzzle = new char[,]
            {
                { '4', ' ', ' ', ' ' },
                { '-', '-', '-', '2' },
                { '3', '-', '-', '-' },
                { '-', '-', '-', '3' }
            };
            puzzle.CheckValid();
            Assert.IsFalse(puzzle.IsValid);
        }

        [TestMethod]
        public void Puzzle_Valid9x9()
        {
            Puzzle puzzle = new Puzzle();
            puzzle.Size = 9;
            puzzle.Symbols.Add('1');
            puzzle.Symbols.Add('2');
            puzzle.Symbols.Add('3');
            puzzle.Symbols.Add('4');
            puzzle.Symbols.Add('5');
            puzzle.Symbols.Add('6');
            puzzle.Symbols.Add('7');
            puzzle.Symbols.Add('8');
            puzzle.Symbols.Add('9');
            puzzle.MyPuzzle = new char[,]
            {
                { '4', '9', '-', '1', '3', '6', '7', '-', '8' },
                { '-', '6', '3', '5', '-', '-', '-', '9', '-' },
                { '5', '-', '-', '-', '2', '9', '3', '6', '4' },
                { '-', '2', '-', '3', '1', '-', '-', '4', '-' },
                { '-', '7', '4', '-', '-', '-', '2', '1', '-' },
                { '-', '-', '1', '-', '6', '4', '-', '8', '-' },
                { '1', '8', '6', '9', '-', '-', '-', '2', '5' },
                { '-', '4', '-', '-', '5', '1', '8', '3', '-' },
                { '3', '-', '9', '4', '8', '-', '-', '7', '-' }
            };
            puzzle.SetupAvailable();
            // check that MyPuzzle has the right values... test a few
            Assert.AreEqual('4', puzzle.MyPuzzle[0, 0]);
            Assert.AreEqual('6', puzzle.MyPuzzle[1, 1]);
            Assert.AreEqual('-', puzzle.MyPuzzle[2, 1]);
            // check size
            Assert.AreEqual(9, puzzle.Size);
            // check that some of the open cells have count of 9
            Assert.AreEqual(9, puzzle.AvailableOptions[3, 2].Count);
            Assert.AreEqual(9, puzzle.AvailableOptions[4, 4].Count);
            Assert.AreEqual(9, puzzle.AvailableOptions[7, 0].Count);
            Assert.AreEqual(9, puzzle.AvailableOptions[8, 8].Count);
            // check the count for row, column and box, each sould have found of 3
            Assert.AreEqual(7, puzzle.Rows[0].Count);
            Assert.AreEqual(4, puzzle.Rows[4].Count);
            Assert.AreEqual(4, puzzle.Rows[5].Count);
            Assert.AreEqual(6, puzzle.Rows[6].Count);
            Assert.AreEqual(4, puzzle.Columns[0].Count);
            Assert.AreEqual(5, puzzle.Columns[3].Count);
            Assert.AreEqual(4, puzzle.Columns[6].Count);
            Assert.AreEqual(8, puzzle.Columns[7].Count);
            Assert.AreEqual(5, puzzle.Boxes[0].Count);
            Assert.AreEqual(6, puzzle.Boxes[2].Count);
            Assert.AreEqual(6, puzzle.Boxes[6].Count);
            Assert.AreEqual(5, puzzle.Boxes[8].Count);
            puzzle.CheckValid();
            Assert.IsTrue(puzzle.IsValid);
        }

        [TestMethod]
        public void Puzzle_Invalid9x9()
        {
            Puzzle puzzle = new Puzzle();
            puzzle.Size = 9;
            puzzle.Symbols.Add('1');
            puzzle.Symbols.Add('2');
            puzzle.Symbols.Add('3');
            puzzle.Symbols.Add('4');
            puzzle.Symbols.Add('5');
            puzzle.Symbols.Add('6');
            puzzle.Symbols.Add('7');
            puzzle.Symbols.Add('8');
            puzzle.Symbols.Add('9');
            puzzle.MyPuzzle = new char[,]
            {
                { '4', '9', '-', '1', '3', '6', '7', '-', '5' },
                { '-', '6', '3', '5', '-', '-', '-', '9', '-' },
                { '5', '-', '-', '-', '2', '9', '3', '6', '4' },
                { '-', '2', '-', '3', '1', '-', '-', '4', '-' },
                { '-', '7', '4', '-', '-', '-', '2', '1', '-' },
                { '-', '-', '1', '-', '6', '4', '-', '8', '-' },
                { '1', '-', '6', '9', '-', '-', '-', '2', '8' },
                { '-', '4', '-', '-', '5', '1', '8', '-', '-' },
                { '3', '-', '9', '4', '8', '-', '-', '7', '-' }
            };
            puzzle.CheckValid();
            Assert.IsFalse(puzzle.IsValid);
        }
    }
}
