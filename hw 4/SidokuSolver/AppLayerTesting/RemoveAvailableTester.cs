﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

using AppLayer;

namespace AppLayerTesting
{
    [TestClass]
    public class RemoveAvailableTester
    {
        [TestMethod]
        public void RemoveAvailable_Valid()
        {
            Puzzle puzzle = new Puzzle();
            puzzle.Size = 4;
            puzzle.Symbols.Add('1');
            puzzle.Symbols.Add('2');
            puzzle.Symbols.Add('3');
            puzzle.Symbols.Add('4');
            puzzle.MyPuzzle = new char[,]
            {
                { '2', '-', '3', '1' },
                { '1', '3', '-', '4' },
                { '3', '1', '4', '-' },
                { '-', '2', '1', '3' }
            };
            puzzle.SetupAvailable();
            Assert.AreEqual(4, puzzle.AvailableOptions[0, 1].Count);
            Assert.AreEqual(4, puzzle.AvailableOptions[1, 2].Count);
            Strategy strategy = new RemoveAvailable() { _puzzle = puzzle };
            bool progress = strategy.Run();
            // assume that the 0,1 position has been updated, then move to the next (1,2), then the next (2,3)
            // and then the last at 3,0.
            Assert.AreEqual(1, puzzle.AvailableOptions[0, 1].Count);
            Assert.AreEqual(1, puzzle.AvailableOptions[1, 2].Count);
            Assert.AreEqual(1, puzzle.AvailableOptions[2, 3].Count);
            Assert.AreEqual(1, puzzle.AvailableOptions[3, 0].Count);
            // since things were updated then progress should have been made.
            Assert.IsTrue(progress);
            // try it again
            progress = strategy.Run();
            // since everything has already been updated then this should mean we are good to go to the next level.
            Assert.IsFalse(progress);
        }
    }
}
