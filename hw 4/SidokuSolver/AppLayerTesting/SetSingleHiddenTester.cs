﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using AppLayer;

namespace AppLayerTesting
{
    [TestClass]
    public class SetSingleHiddenTester
    {
        [TestMethod]
        public void SetSingleHidden_Valid()
        {
            Puzzle puzzle = new Puzzle();
            puzzle.Size = 4;
            puzzle.Symbols.Add('1');
            puzzle.Symbols.Add('2');
            puzzle.Symbols.Add('3');
            puzzle.Symbols.Add('4');
            puzzle.MyPuzzle = new char[,]
            {
                { '-', '-', '-', '1' },
                { '1', '-', '-', '4' },
                { '-', '-', '4', '-' },
                { '-', '2', '-', '-' }
            };
            puzzle.SetupAvailable();
            // im going to focus on the bottom right 2 squares for this strategy.
            Assert.AreEqual(4, puzzle.AvailableOptions[0, 0].Count);
            Assert.AreEqual(4, puzzle.AvailableOptions[0, 1].Count);
            Strategy strategy = new RemoveAvailable() { _puzzle = puzzle };
            bool progress = strategy.Run();
            // assume that the 2,3 and 3,3 positions have been updated
            Assert.AreEqual(3, puzzle.AvailableOptions[0, 0].Count);
            Assert.AreEqual(1, puzzle.AvailableOptions[2, 0].Count);
            // since things were updated then progress should have been made.
            Assert.IsTrue(progress);
            // now change strategy to set single hidden algorithm
            strategy = new SetSingleHidden() { _puzzle = puzzle };
            // 2,3 should have options for a 2 and 3... if we ran the set single it would set 3,3 since it just has 3
            // but we want to test that the hidden single (the 2 in 2,3 cell) works
            progress = strategy.Run();
            Assert.AreEqual('2', puzzle.MyPuzzle[0, 0]);
            Assert.IsTrue(progress);
        }
    }
}
