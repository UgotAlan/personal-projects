﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AppLayer;

namespace AppLayerTesting
{
    [TestClass]
    public class UndoGuessTester
    {
        [TestMethod]
        public void UndoTest_Valid()
        {
            Puzzle puzzle = new Puzzle();
            puzzle.Size = 4;
            puzzle.Symbols.Add('1');
            puzzle.Symbols.Add('2');
            puzzle.Symbols.Add('3');
            puzzle.Symbols.Add('4');
            puzzle.MyPuzzle = new char[,]
            {
                { '2', '-', '-', '-' },
                { '-', '-', '-', '4' },
                { '3', '-', '4', '-' },
                { '-', '2', '1', '3' }
            };
            puzzle.SetupAvailable();
            Assert.AreEqual(4, puzzle.AvailableOptions[0, 1].Count);
            Strategy strategy = new RemoveAvailable() { _puzzle = puzzle };
            bool progress = strategy.Run();
            Assert.AreEqual(3, puzzle.AvailableOptions[0, 1].Count);
            // since things were updated then progress should have been made.
            Assert.IsTrue(progress);
            // now change strategy to set random algorithm
            strategy = new SetRandom() { _puzzle = puzzle };
            // try it again
            progress = strategy.Run();
            // since everything has already been updated then this should mean we are good to go to the next level.
            Assert.IsTrue(progress);
            // make sure that the symbol picked randomly that was placed in the stack is the same in the grid.
            char savedSymbol = puzzle.SavedSymbol.Peek();
            Assert.AreEqual(savedSymbol, puzzle.MyPuzzle[0, 1]);
            Assert.AreEqual(1, puzzle.SavedPuzzleState.Count);
            Assert.AreEqual(1, puzzle.SavedAvailableOptions.Count);
            Assert.AreEqual(1, puzzle.SavedSymbol.Count);
            // now undo the guess
            strategy = new UndoGuess() { _puzzle = puzzle };
            progress = strategy.Run();
            Assert.IsTrue(progress);
            Assert.IsFalse(puzzle.AvailableOptions[0, 1].Contains(savedSymbol));
            Assert.AreEqual(2, puzzle.AvailableOptions[0, 1].Count);
            Assert.AreEqual(0, puzzle.SavedPuzzleState.Count);
            Assert.AreEqual(0, puzzle.SavedAvailableOptions.Count);
            Assert.AreEqual(0, puzzle.SavedSymbol.Count);
        }
    }
}
