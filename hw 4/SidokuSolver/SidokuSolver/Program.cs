﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AppLayer;
using System.IO;

namespace SidokuSolver
{
    class Program
    {
        static void Main(string[] args)
        {
            char response = 'y';
            do
            {
                PuzzleSolver solver = new PuzzleSolver();
                bool valid = false;
                string inFilename = null;
                string outFilename = null;
                while (!valid)
                {
                    inFilename = GetFilename("Puzzle filename to load");
                    if (File.Exists(inFilename))
                        valid = true;
                    else
                        Console.WriteLine("Not a valid file. Try again.");
                }
                outFilename = GetFilename("Output filename");
                solver.LoadPuzzle(inFilename);
                solver.Solve();
                solver.Save(outFilename);

                Console.Write("Would you like to run another puzzle? (y/n): ");
                response = Console.ReadKey().KeyChar;
                Console.ReadLine();
                Console.WriteLine();
            } while (response == 'Y' || response == 'y');
            
        }

        private static string GetFilename(string str)
        {
            Console.Write($"{str}: ");
            return Console.ReadLine();
        }
    }
}
