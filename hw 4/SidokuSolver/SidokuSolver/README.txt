﻿I was planning on doing 5 algorithms before doing the guessing but was not able to get those
in yet so I will impliment those for homework 5. There were two files that used a lot of guessing
so I limited the guesses to 5,000 to keep the program from running too long and marked those
puzzles as unsolvable. I will change that with the new strategies as I hope they will reduce the
number of guesses, thus improving the speed of the program.

All of the provided pratice puzzles are located at "Input/Puzzle-name.txt" inside the bin folder for
the program.